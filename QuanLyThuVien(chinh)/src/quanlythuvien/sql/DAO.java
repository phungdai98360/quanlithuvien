package quanlythuvien.sql;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import quanlythuvien.model.DocGia;
import quanlythuvien.model.Sach;


public class DAO {

    Connection conn;

    public DAO() {
        try {
            String uRL = "jdbc:sqlserver://localhost:1433;databaseName=QLThuVien";
            String user = "sa";
            String pass = "mixaolanot123";
            conn = DriverManager.getConnection(uRL, user, pass);
            System.out.println("Ket noi sql thanh cong");
        } catch (Exception e) {
            System.out.println("Loi ket noi sql");
        }
    }

    //Doc danh sach Sach tu SQL sang java
    public ArrayList<Sach> getListSach() {
        ArrayList<Sach> list = new ArrayList<Sach>();
        String sql = "select * from Sach";
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Sach s = new Sach();
                s.setMasach(rs.getString(1));
                s.setTensach(rs.getString(2));
                s.setSotrang(rs.getInt(3));
                s.setNamxuatban(rs.getString(4));
                s.setDongia(rs.getFloat(5));
                s.setNhaxuatban(rs.getString(6));
                s.setTheloai(rs.getString(7));
                s.setVitri(rs.getString(8));
                s.setTrangthai(rs.getInt(9));
                list.add(s);
            }
        } catch (Exception e) {
            System.out.println("Loi getListSach");
            e.printStackTrace();
        }
        return list;
    }
//public ArrayList<Sach> getListSach1(float dg) {
//        ArrayList<Sach> list = new ArrayList<Sach>();
//        String sql = "select * from Sach where DONGIA>?";
//        try {
//            Sach s = new Sach();
//            PreparedStatement ps = conn.prepareStatement(sql);
//            ps.setFloat(1, dg);
////            ps.setFloat(1,s.getDongia() );
//            ResultSet rs = ps.executeQuery();
//            while (rs.next()) {
//                //Sach s = new Sach();
//                s.setMasach(rs.getString(1));
//                s.setTensach(rs.getString(2));
//                s.setSotrang(rs.getInt(3));
//                s.setNamxuatban(rs.getString(4));
//                s.setDongia(rs.getFloat(5));
//                s.setNhaxuatban(rs.getString(6));
//                s.setTheloai(rs.getString(7));
//                s.setVitri(rs.getString(8));
//                s.setTrangthai(rs.getInt(9));
//                list.add(s);
//            }
//        } catch (Exception e) {
//            System.out.println("Loi getListSach");
//            e.printStackTrace();
//        }
//        return list;
//    }
    //Thêm sách vào SQL
    public ArrayList<Sach> getListSach2() {
        ArrayList<Sach> list = new ArrayList<Sach>();
        String sql = "select MASACH,count(MADG) from MuonTra\n" +
                       "group by MASACH";
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                
                Sach s = new Sach();
                s.setMasach(rs.getString(1));
                s.setSo(rs.getInt(2));

                list.add(s);
            }
        } catch (Exception e) {
            System.out.println("Loi getListSach");
            e.printStackTrace();
        }
        return list;
    }
    public ArrayList<Sach> getListSach3() {
        ArrayList<Sach> list = new ArrayList<Sach>();
        String sql = "select top 1 MASACH,count(MADG) from MuonTra\n" +
                      "group by MASACH";
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                
                Sach s = new Sach();
                s.setMasach(rs.getString(1));
                s.setSo(rs.getInt(2));

                list.add(s);
            }
        } catch (Exception e) {
            System.out.println("Loi getListSach");
            e.printStackTrace();
        }
        return list;
    }
    
    public boolean addSach(Sach s) {
        String sql = "insert into Sach(MASACH, TENSACH, SOTRANG, NAMXUATBAN, DONGIA, NHAXUATBAN, THELOAI, VITRI, TRANGTHAI) values(?,?,?,?,?,?,?,?,?)";
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, s.getMasach());
            ps.setString(2, s.getTensach());
            ps.setInt(3, s.getSotrang());
            ps.setString(4, s.getNamxuatban());
            ps.setFloat(5, s.getDongia());
            ps.setString(6, s.getNhaxuatban());
            ps.setString(7, s.getTheloai());
            ps.setString(8, s.getVitri());
            ps.setInt(9, s.getTrangthai());
            return ps.executeUpdate() > 0;
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Lỗi!");
        }
        return false;
    }

    //Update sách 
    public boolean suaSach(Sach s) {
        String sql = "update Sach set TENSACH = ?, SOTRANG = ?, NAMXUATBAN = ?, DONGIA = ?, NHAXUATBAN = ?, THELOAI = ?, VITRI = ?, TRANGTHAI = ? WHERE MASACH = ?";
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, s.getTensach());
            ps.setInt(2, s.getSotrang());
            ps.setString(3, s.getNamxuatban());
            ps.setFloat(4, s.getDongia());
            ps.setString(5, s.getNhaxuatban());
            ps.setString(6, s.getTheloai());
            ps.setString(7, s.getVitri());
            ps.setInt(8, s.getTrangthai());
            ps.setString(9, s.getMasach());
            //ps.setString(10, ms);
            return ps.executeUpdate() > 0;
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Lỗi!");
        }
        return false;
    }

    //Xóa sách
    public boolean xoaSach(Sach s) {

            String sql = "delete from Sach where MASACH = ?";
            try {
                PreparedStatement ps = conn.prepareStatement(sql);
                ps.setString(1, s.getMasach());
                int yn = JOptionPane.showConfirmDialog(null, "Bạn muốn xóa?", "Thông báo xóa", JOptionPane.YES_NO_OPTION);
                if (yn == JOptionPane.YES_OPTION) {
                    return ps.executeUpdate() > 0;
                } else {
                    return false;
                }

            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "Lỗi");
            }
        return false;

    }

    //Lấy trạn thái của sách trả về -1 nếu không có sách cần tìm
    public int getTrangThaiSach(String masach) {
        int trangthai = -1;
        String sql = "select * from Sach where MASACH = ? ";
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, masach);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                trangthai = rs.getInt(9);
            }
            return trangthai;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return -1;
    }

    //Lấy danh sách độc giả từ SQL
    public ArrayList<DocGia> getListDG() {
        ArrayList<DocGia> list = new ArrayList<DocGia>();
        String sql = "select * from DocGia";
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                DocGia dg = new DocGia();
                dg.setMadg(rs.getString(1));
                dg.setHo(rs.getString(2));
                dg.setTen(rs.getString(3));
                dg.setCmnd(rs.getString(4));
                dg.setNgaysinh(rs.getString(5));
                dg.setPhai(rs.getString(6));
                dg.setDiachi(rs.getString(7));
                list.add(dg);
            }
        } catch (Exception e) {
        }
        return list;
    }

    //Thêm độc giả váo SQL
    public boolean addDocGia(DocGia dg) {
        String sql = "insert into DocGia(MADG, HO, TEN, CMND, NGAYSINH, PHAI, DIACHI) values(?,?,?,?,?,?,?)";
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, dg.getMadg());
            ps.setString(2, dg.getHo());
            ps.setString(3, dg.getTen());
            ps.setString(4, dg.getCmnd());
            ps.setString(5, dg.getNgaysinh());
            ps.setString(6, dg.getPhai());
            ps.setString(7, dg.getDiachi());
            return ps.executeUpdate() > 0;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    //Sửa độc giả
    public boolean suaDocGia(DocGia dg, String mdg) {
        String Sql = "update DocGia set MADG=?, HO=?, TEN=?, CMND=?, NGAYSINH=?, PHAI=?, DIACHI=? WHERE MADG=?";
        try {
            PreparedStatement ps = conn.prepareStatement(Sql);
            ps.setString(1, dg.getMadg());
            ps.setString(2, dg.getHo());
            ps.setString(3, dg.getTen());
            ps.setString(4, dg.getCmnd());
            ps.setString(5, dg.getNgaysinh());
            ps.setString(6, dg.getPhai());
            ps.setString(7, dg.getDiachi());
            ps.setString(8, mdg);
            return ps.executeUpdate() > 0;
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Lỗi!");
        }
        return false;
    }

    //Xóa một độc giả
    public boolean xoaDocGia(String mdg) {
        if (mdg.equals("")) {
            JOptionPane.showMessageDialog(null, "Bạn chưa chọn sách nào!");
        } else {

            String sql = "delete from DocGia where MADG = ?";
            try {
                PreparedStatement ps = conn.prepareStatement(sql);
                ps.setString(1, mdg);
                int yn = JOptionPane.showConfirmDialog(null, "Bạn muốn xóa?", "Thông báo xóa", JOptionPane.YES_NO_OPTION);
                if (yn == JOptionPane.YES_OPTION) {
                    return ps.executeUpdate() > 0;
                } else {
                    return false;
                }

            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "Lỗi");
                e.printStackTrace();
            }
        }
        return false;
    }

    //Kiểm tra mã độc giả có tồn tại không
    public boolean checkMaDocGia(String mdg) {
        String sql = "SELECT * FROM DocGia where MADG = ?";
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, mdg);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    //Kiểm tra mã độc giả có mượn quá 3 cuốn sách và có giữ cuốn sách nào quá hạn không    
    public int checkDGMuonQua3Cuon(String mdg) {
        int soluong = -1;
        String sql = "SELECT COUNT(MADG) FROM MuonTra WHERE TRANGTHAI = 1 AND MADG = ?";
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, mdg);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return soluong = Integer.parseInt(rs.getString(1));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return soluong;
    }

    //Kiểm tra có mã sách trong thư viện không
    public boolean checkSach(String ms) {
        String sql = "SELECT * FROM Sach where MASACH = ?";
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, ms);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    //Kiểm tra trạng thái sách
    public boolean checkTTSach(String ms) {
        String sql = "SELECT * FROM Sach where MASACH = ? and TRANGTHAI = 0";
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, ms);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    //Update trạng thái trong bang sách
    public boolean updateTTSach(int tt, String ms) {
        String sql = "update Sach  set TRANGTHAI = ? where MASACH = ?";
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, tt);
            ps.setString(2, ms);
            return ps.executeUpdate() > 0;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
    //Chèn vào bảng độc giả vào bảng mượn trả
    public boolean insertMuonTra(String ms, String mdg, Date ngaymuon, Date ngaytra, int trangthai) {
        String sql = "insert into MuonTra(MASACH, MADG, NGAYMUON, NGAYTRA, TRANGTHAI) VALUES(?,?,?,?,?)";
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, ms);
            ps.setString(2, mdg);
            ps.setDate(3, ngaymuon);
            ps.setDate(4, ngaytra);
            ps.setInt(5, trangthai);
            return ps.executeUpdate() > 0;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
    
    //Update TRANGTHAI trong bang MuonTra
    public boolean updateTrangThaiMuonTra(String mdg, String ms) {
        String sql = "update MuonTra set TRANGTHAI = 0 where MADG = ? and MASACH = ?";
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, mdg);
            ps.setString(2, ms);
            return ps.executeUpdate() >0 ;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    //Kiểm tra độc giả có mượn quá hạn không, nếu có thì sẽ không có mượn tiếp
    public boolean checkQuaHan(String mdg) {
        Date ngaytra = null;
        String sql = "select * from MuonTra where MADG = ? and TRANGTHAI = 1";
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, mdg);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                DateFormat df = new SimpleDateFormat("dd/mm/yyyy");
                ngaytra = rs.getDate(4);
                long nht = System.currentTimeMillis();
                Date ngayhientai = new Date(nht);
                int i = ngaytra.compareTo(ngayhientai);
                if (i < 0) {
//                System.out.println("Qua han");
                    return false;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }
    public ArrayList<Sach> getListSachMotDGDangMuon(String mdg){
        ArrayList<Sach> list = new ArrayList<Sach>();
        String sql = "SELECT s1.MASACH, TENSACH, SOTRANG, NAMXUATBAN, DONGIA, NHAXUATBAN, THELOAI FROM Sach AS s1, (SELECT MASACH, TRANGTHAI FROM dbo.MuonTra WHERE MADG = ?) AS s2 WHERE s1.MASACH = s2.MASACH and s2.TRANGTHAI = 1";
        try{
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, mdg);
            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                Sach s = new Sach();
                s.setMasach(rs.getString(1));
                s.setTensach(rs.getString(2));
                s.setSotrang(rs.getInt(3));
                s.setNamxuatban(rs.getString(4));
                s.setDongia(rs.getFloat(5));
                s.setNhaxuatban(rs.getString(6));
                s.setTheloai(rs.getString(7));
//                s.setTheloai(rs.getString(8));
//                s.setVitri(rs.getString(9));
//                s.setTrangthai(rs.getInt(10));
                list.add(s);
                
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        
        return list;
    }
    
    //Thống kê sách khả dụng
    public ArrayList<Sach> getListSachKhaDung(){
         ArrayList<Sach> list = new ArrayList<Sach>();
        String sql = "select * from Sach where TRANGTHAI = 0";
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Sach s = new Sach();
                s.setMasach(rs.getString(1));
                s.setTensach(rs.getString(2));
                s.setSotrang(rs.getInt(3));
                s.setNamxuatban(rs.getString(4));
                s.setDongia(rs.getFloat(5));
                s.setNhaxuatban(rs.getString(6));
                s.setTheloai(rs.getString(7));
                s.setVitri(rs.getString(8));
                s.setTrangthai(rs.getInt(9));
                list.add(s);
            }
        } catch (Exception e) {
            System.out.println("Loi getListSach");
            e.printStackTrace();
        }
        return list;
    }
    //Thống kê độc giả mượn sách quá hạn
    public ArrayList<DocGia> getListDocGiaQuaHan(){
        ArrayList<DocGia> list = new ArrayList<DocGia>();
        String sql = "SELECT dg1.MADG, HO, dg1.TEN, dg1.CMND, dg1.NGAYSINH, dg1.PHAI, dg1.DIACHI FROM dbo.DocGia AS dg1, (SELECT MADG FROM dbo.MuonTra WHERE TRANGTHAI = 1 AND YEAR(NGAYTRA) >= YEAR(GETDATE()) AND MONTH(NGAYTRA) >= MONTH(GETDATE()) GROUP BY MADG) AS dg2 WHERE  dg1.MADG = dg2.MADG";
        try{
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                DocGia dg = new DocGia();
                dg.setMadg(rs.getString(1));
                dg.setHo(rs.getString(2));
                dg.setTen(rs.getString(3));
                dg.setCmnd(rs.getString(4));
                dg.setNgaysinh(rs.getString(5));
                dg.setPhai(rs.getString(6));
                dg.setDiachi(rs.getString(7));
                list.add(dg);
            }
        }
        catch(Exception e){
            System.out.println("Loi getListDocGiaMuonQuaHan()");
            e.printStackTrace();
        }
        return list;
    }
    
    
    
    
    
    
    
//    public static void main(String args []) {
//        ArrayList<Sach> ds = new DAO().getListSach2();
//        for (int i = 0; i < ds.size(); i++) {
//            System.out.println(ds.get(i).getMatheloai());
//        }
//    }
}
